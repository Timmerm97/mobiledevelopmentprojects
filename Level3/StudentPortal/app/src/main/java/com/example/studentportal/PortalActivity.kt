package com.example.studentportal

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.recyclerview.widget.StaggeredGridLayoutManager

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

const val CREATE_PORTAL_REQUEST_CODE = 100
private val tabBuilder = CustomTabsIntent.Builder()
private val tabIntent = tabBuilder.build()

class PortalActivity : AppCompatActivity() {

    private val portals = arrayListOf<Portal>()
    private val portalAdapter = PortalAdapter(portals,  ::onPortalClick)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initViews()

        fab.setOnClickListener { startAddActivity() }

    }

    private fun startAddActivity() {
        val intent = Intent(this, CreatePortalActivity::class.java)
        startActivityForResult(intent, CREATE_PORTAL_REQUEST_CODE)
    }

    private fun initViews(){

        rvPortals.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        rvPortals.adapter = portalAdapter
        tabBuilder.setShowTitle(true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CREATE_PORTAL_REQUEST_CODE -> {
                    val portal = data!!.getParcelableExtra<Portal>(EXTRA_PORTAL)
                    portals.add(portal)
                    portalAdapter.notifyDataSetChanged()
                } else -> super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun onPortalClick(){

        var btnPortal = findViewById<Button>(R.id.btnPortal)
        val split = btnPortal.text.toString().split("\n")
        tabIntent.launchUrl(this, Uri.parse(split[1]))

    }
}
