package com.example.studentportal

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_create_portal.*

const val EXTRA_PORTAL = "EXTRA_PORTAL"

class CreatePortalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_portal)
        initViews()
    }

    private fun initViews(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.create_portal_title)

        btnAdd.setOnClickListener { onAddClick() }
    }

    private fun onAddClick() {
        if (!etTitle.text.isNullOrBlank() &&
            !etUrl.text.isNullOrBlank()){

            val portal = Portal(
                etTitle.text.toString(),
                etUrl.text.toString()
            )

            val portalActivityIntent = Intent()
            portalActivityIntent.putExtra(EXTRA_PORTAL, portal)
            setResult(Activity.RESULT_OK, portalActivityIntent)
            finish()

        } else {
            Toast.makeText(this, "Both values should be filled in", Toast.LENGTH_SHORT).show()
        }
    }

}
