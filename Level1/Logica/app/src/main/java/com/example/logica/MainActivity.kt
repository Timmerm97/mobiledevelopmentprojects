package com.example.logica

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var et1: String = input1.text.toString()
    private var et2: String = input2.text.toString()
    private var et3: String = input4.text.toString()
    private var et4: String = input4.text.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener { check() }
    }

    private fun check(){

        if (!et1.isEmpty() &&
            !et2.isEmpty() &&
            !et3.isEmpty() &&
            !et4.isEmpty()) {

           if (et1 == "T" &&
               et2 == "F" &&
               et3 == "F" &&
               et4 == "F") {

               Toast.makeText(this, getString(R.string.correct), Toast.LENGTH_SHORT).show()

           }else{
               Toast.makeText(this, getString(R.string.incorrect), Toast.LENGTH_SHORT).show()
           }

        } else {

            Toast.makeText(this, getString(R.string.fill), Toast.LENGTH_SHORT).show()

        }

    }


}
