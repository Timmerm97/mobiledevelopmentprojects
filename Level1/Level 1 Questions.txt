What does the abbreviation ART stands for?
    ART stands for the android runtime.
    Its the default runtime for devices running the level 21 API


What is Android Jetpack?
    Android Jetpack is a collection of android software components


Describe the difference between the fixed, wrap_content and match_constraint setting of the constraint layout?
    fixed: limits the widget to a "fixed" size and expansion
    wrap_content: measures the widget to itself, so does expend against constraints
    match_constraint: limits the size of the widget, but matches even when wrap_content is smaller 


What does the abbreviation DP stand for and why do we need them?
    DP stand for Density-independend Pixels, we need them to to expres layout dimensions and positions


What is the purpose of the string.xml file?
    To avoid hard coded strings in the main_layout.xml and the main_activity.kl, and collect them in one file.


Why is the layout in Android specified by .xml files?  Why not just have the layout in the code (Kotlin or Java)?
    To seperate the design of the app from the code that controllers the app.


In the level 1 example the following code was used: btnConfirm.setOnClickListener What is a listener and what is the underlying design pattern?
    A listener is a interface that includes a single callback method, which is triggered when the button to which the listener is linked to is clicked.
    

