package com.example.popularmovies.api

class MoviesRepository {

    private val moviesApiService: MoviesApiService = MoviesApi.createApi()

    fun getMoviesByYear(year: String) = moviesApiService.getMoviesByYear(year)

}