package com.example.popularmovies.api

import com.example.popularmovies.model.ApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

const val KEY = "5f79c4ad6d008008f5819c3fc95548e8"

interface MoviesApiService {

    @GET("discover/movie?api_key=${KEY}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&")
    fun getMoviesByYear(@Query("primary_release_year") year: String): Call<ApiResponse>

}