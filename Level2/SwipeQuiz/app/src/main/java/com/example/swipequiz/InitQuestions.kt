package com.example.swipequiz

data class InitQuestions(val primary: String){
    companion object{
        val questions = listOf(
            Question("Is wood plastic?",false),
            Question("Can tables fly?",false),
            Question("Are banana\\'s yellow?",true),
            Question("Does a bear shit in the woods?",true))

    }
}