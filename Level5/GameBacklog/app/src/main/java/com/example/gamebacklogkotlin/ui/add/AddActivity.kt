package com.example.gamebacklogkotlin.ui.add

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.gamebacklogkotlin.R

import kotlinx.android.synthetic.main.activity_add.*
import kotlinx.android.synthetic.main.content_add.*

class AddActivity : AppCompatActivity() {

    private lateinit var addViewModel: AddViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        setSupportActionBar(toolbar)

        initViews()
        initViewModel()
    }

    private fun initViews() {

    }

    private fun initViewModel(){
        addViewModel = ViewModelProviders.of(this).get(AddViewModel::class.java)
        addViewModel.game.value = intent.extras?.getParcelable(EXTRA_GAME)!!

        addViewModel.game.observe(this, Observer { game ->
            if(game != null) {
                etTitle.setText(game.title)
                etPlatform.setText(game.platform)
                etDay.setText(game.release)
                etMonth.setText(game.release)
                etYear.setText(game.release)
            }
        })

    }

    companion object {
        const val EXTRA_GAME = "EXTRA_GAME"
    }

}
