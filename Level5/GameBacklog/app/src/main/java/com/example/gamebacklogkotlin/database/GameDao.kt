package com.example.gamebacklogkotlin.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.gamebacklogkotlin.model.Game

@Dao
interface GameDao {

    @Query("SELECT * FROM Games")
    fun getAllGames(): LiveData<List<Game>>

    @Insert
    suspend fun insertGame(game: Game)

    @Delete
    suspend fun deleteGame(game: Game)




}