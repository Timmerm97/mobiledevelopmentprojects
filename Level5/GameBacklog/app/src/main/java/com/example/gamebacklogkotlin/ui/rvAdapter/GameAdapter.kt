package com.example.gamebacklogkotlin.ui.rvAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.gamebacklogkotlin.R
import com.example.gamebacklogkotlin.model.Game

public class GameAdapter(private val games: List<Game>) : RecyclerView.Adapter<GameAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.rv_item_game, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return games.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(games[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        private val tvPlatform: TextView = itemView.findViewById(R.id.tvPlatform)
        private val tvRelease: TextView = itemView.findViewById(R.id.tvRelease)

        fun bind(game: Game) {
            tvTitle.text = game.title
            tvPlatform.text = game.platform
            tvRelease.text = game.release
        }

    }

}