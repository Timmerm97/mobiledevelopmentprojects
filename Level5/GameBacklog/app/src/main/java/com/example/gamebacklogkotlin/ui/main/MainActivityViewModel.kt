package com.example.gamebacklogkotlin.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.gamebacklogkotlin.database.GameRepository

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val gameRepository = GameRepository(application.applicationContext)

    val game = gameRepository.getAllGames()

}