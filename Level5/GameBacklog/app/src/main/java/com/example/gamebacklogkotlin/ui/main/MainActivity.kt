package com.example.gamebacklogkotlin.ui.main

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.gamebacklogkotlin.R
import com.example.gamebacklogkotlin.ui.add.AddActivity

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.rv_item_game.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        initViews()
        initViewModels()
    }

    private fun initViews(){
        fab.setOnClickListener {
            val intent = Intent(this, AddActivity::class.java)
            intent.putExtra(AddActivity.EXTRA_GAME, mainActivityViewModel.game.value)
            startActivity(intent)
        }
    }

    private fun initViewModels() {
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        mainActivityViewModel.game.observe(this, Observer { game ->
            if (game != null) {
                tvPlatform.text = game.platform
                tvTitle.text = game.title
                tvRelease.text = game.release
            }
        })
    }
}
