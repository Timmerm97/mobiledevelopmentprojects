package com.example.catalogofentertainment.api

private const val API_KEY = "AIzaSyBQ7xNDio1Bma6bI99wNn6SLJizBII8YzY"

class BooksApiRepository {

    private val booksApi: BooksApiService = BooksApi.createApi()

    fun getBook(isbn: String) = booksApi.getBook("isbn:$isbn", API_KEY)
    fun getDefaultBook() = booksApi.getDefaultBook()


}