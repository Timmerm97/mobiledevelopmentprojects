package com.example.rockpaperscissors.model


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(tableName = "game_table")
data class Game (

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,

    @ColumnInfo(name = "result")
    var result: Result,

    @ColumnInfo(name = "date")
    var date: Date,

    @ColumnInfo(name = "npcPlay")
    var npcPlay: Play,

    @ColumnInfo(name = "userPlay")
    var userPlay: Play

) : Parcelable