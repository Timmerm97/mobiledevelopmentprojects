package com.example.rockpaperscissors.ui.gameActivity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.rockpaperscissors.R
import com.example.rockpaperscissors.database.GameRepository
import com.example.rockpaperscissors.model.Game
import com.example.rockpaperscissors.model.Play
import com.example.rockpaperscissors.model.Result
import com.example.rockpaperscissors.ui.historyActivity.HistoryActivity
import kotlinx.android.synthetic.main.activity_game.toolbar
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

private const val ADD_HISTORY_REQUEST_CODE = 1234

class GameActivity : AppCompatActivity() {

    private lateinit var gameRepository: GameRepository
    private val mainScope = CoroutineScope(Dispatchers.Main)
    private var stats: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        setSupportActionBar(toolbar)

        gameRepository = GameRepository(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.app_name)

        initViews()
    }

    private fun initViews(){

        btnRock.setOnClickListener{ gameOn(Play.ROCK) }
        btnPaper.setOnClickListener { gameOn(Play.PAPER) }
        btnScissor.setOnClickListener { gameOn(Play.SCISSOR) }

    }

    private fun gameOn(userPlay: Play) {
        val npcPlay = Play.getRandom()
        val result = getWinner(userPlay, npcPlay)

        val game = Game(
            date = Date(),
            result = result,
            npcPlay =  npcPlay,
            userPlay = userPlay
        )

        addGame(game)
        drawResult(game)


    }

    private fun addGame(game: Game) {
            mainScope.launch {
                withContext(Dispatchers.IO) {
                    gameRepository.insertGame(game)
                }
            }
    }

    private fun getWinner(userPlay: Play, npcPlay: Play): Result{

        if(userPlay == npcPlay){
            return Result.DRAW
        }
        return when(userPlay) {
            Play.ROCK -> if (npcPlay == Play.PAPER) Result.LOSE else Result.WIN
            Play.PAPER -> if (npcPlay == Play.SCISSOR) Result.LOSE else Result.WIN
            Play.SCISSOR -> if (npcPlay == Play.ROCK) Result.LOSE else Result.WIN
        }

    }

    private fun drawResult(game: Game){

        var result = when {
            game.result == Result.WIN -> "You " + game.result.string + "!"
            game.result == Result.LOSE -> "Computer wins!"
            else -> game.result.string
        }
        tvOutcome.text = result
        ivPC.setImageDrawable(game.npcPlay.getDrawable(this))
        ivPlayer.setImageDrawable(game.userPlay.getDrawable(this))
        drawStats()

    }

    private fun drawStats() {

        mainScope.launch {
            withContext(Dispatchers.IO) {
                stats = gameRepository.getStatistics()
            }

            tvStats.text = stats

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_history_activity -> {
                val intent = Intent(this, HistoryActivity::class.java)
                startActivityForResult(intent, ADD_HISTORY_REQUEST_CODE)
                true
            } else -> super.onOptionsItemSelected(item)
        }
    }


}


