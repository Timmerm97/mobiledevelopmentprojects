package com.example.rockpaperscissors.ui.historyActivity

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rockpaperscissors.R
import com.example.rockpaperscissors.model.Game
import com.example.rockpaperscissors.model.Result
import kotlinx.android.synthetic.main.item_game.view.*

class HistoryAdapter(private val games: List<Game>, private val context: Context) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_game, parent, false)
        )
    }

    override fun getItemCount(): Int {
            return games.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(games[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(game: Game) {

            var result = when {
                game.result == Result.WIN -> "You " + game.result.string + "!"
                game.result == Result.LOSE -> "Computer wins!"
                else -> game.result.string
            }

            itemView.tvOutcome.text = result
            itemView.tvDate.text = game.date.toString()
            itemView.ivPC.setImageDrawable(game.npcPlay.getDrawable(context))
            itemView.ivPlayer.setImageDrawable(game.userPlay.getDrawable(context))


        }

    }

}