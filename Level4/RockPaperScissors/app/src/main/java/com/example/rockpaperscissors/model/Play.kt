package com.example.rockpaperscissors.model

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.example.rockpaperscissors.R
import java.util.*


enum class Play(val string: String) {
    ROCK("Rock"),
    PAPER("Paper"),
    SCISSOR("Scissor");

    override fun toString(): String {
        return string
    }

    fun getDrawable(context: Context): Drawable? {
        val drawableRef = when(this) {
            ROCK -> R.drawable.rock
            PAPER -> R.drawable.paper
            SCISSOR -> R.drawable.scissors
        }
        return ContextCompat.getDrawable(context, drawableRef)
    }

    companion object{
        fun getRandom(): Play{
            val random = Random()
            return Play.values()[random.nextInt(Play.values().size)]
        }
    }

}

