package com.example.rockpaperscissors.model

import androidx.room.TypeConverter
import java.util.*

class TypeConverters {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }

    @TypeConverter
    fun toOrdinal(play: Play): Int {
        return play.ordinal
    }

    @TypeConverter
    fun toPlay(ordinal: Int): Play?{
        return Play.values().first { it.ordinal == ordinal }
    }

    @TypeConverter
    fun toResult(ordinal: Int): Result?{
        return Result.values().first{ it.ordinal == ordinal}
    }

    @TypeConverter
    fun toString(result: Result): Int {
        return result.ordinal
    }
}