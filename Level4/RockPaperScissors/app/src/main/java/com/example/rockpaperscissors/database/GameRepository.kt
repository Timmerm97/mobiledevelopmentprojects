package com.example.rockpaperscissors.database

import android.content.Context
import com.example.rockpaperscissors.model.Game
import com.example.rockpaperscissors.model.Result

class GameRepository(context: Context) {

    private val gameDao: GameDao

    init {
        val database = GameRoomDatabase.getDatabase(context)
        gameDao = database!!.gameDao()
    }

    suspend fun getAllGames(): List<Game> {
        return gameDao.getAllGames()
    }

    suspend fun insertGame(game: Game) {
        gameDao.insertGame(game)
    }

    suspend fun deleteAllGames() {
        gameDao.deleteAllGames()
    }


    suspend fun getStatistics(): String {
        val win = gameDao.getGameCount(Result.WIN)
        val lose = gameDao.getGameCount(Result.LOSE)
        val draw = gameDao.getGameCount(Result.DRAW)
        return "Win: $win Draw: $draw Lose: $lose"
    }


}