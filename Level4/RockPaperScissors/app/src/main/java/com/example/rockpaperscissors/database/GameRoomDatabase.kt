package com.example.rockpaperscissors.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.rockpaperscissors.model.Game
import com.example.rockpaperscissors.model.TypeConverters

@Database(entities = [Game::class], version = 2, exportSchema = false)
@androidx.room.TypeConverters(TypeConverters::class)
abstract class GameRoomDatabase : RoomDatabase(){

    abstract fun gameDao() : GameDao

    companion object {
        private const val DATABASE_NAME = "GAME_DATABASE"

        @Volatile
        private var GameRoomDatabaseInstance: GameRoomDatabase? = null


        fun getDatabase(context: Context) : GameRoomDatabase? {
            if (GameRoomDatabaseInstance == null) {
                synchronized(GameRoomDatabase::class.java) {
                    if (GameRoomDatabaseInstance == null) {
                        GameRoomDatabaseInstance =
                            Room.databaseBuilder(context.applicationContext,
                                GameRoomDatabase::class.java,
                                DATABASE_NAME
                            ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return GameRoomDatabaseInstance
        }

    }
}