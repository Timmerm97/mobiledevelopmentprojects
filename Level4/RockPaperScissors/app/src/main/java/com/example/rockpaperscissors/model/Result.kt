package com.example.rockpaperscissors.model

enum class Result(val string: String) {
    WIN("Win"),
    DRAW("Draw"),
    LOSE("Lose");

    override fun toString(): String {
        return string
    }
}